﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TP_AppBackEnd.Model;

namespace TP_AppBackEnd.Controllers
{
    [Route("api")]
    public class ApiController : Controller
    {
        [HttpPost("checkLogin")]
        public Teacher CheckLogin([FromBody]Teacher teacherRequest) => teacherRequest.CheckUser();

        [HttpPost("createAccount")]
        public int CreateAccount([FromBody]Teacher teacherRequest) => teacherRequest.CreateAccount();

        [HttpGet("getClassToday/{idUser}")]
        public List<Lesson> GetClassToday(int idUser) => Lesson.GetClassToday(idUser);

        [HttpGet("getJobToday/{idUser}")]
        public List<JobDelivery> GetJobDeliveryToday(int idUser) => JobDelivery.GetJobDeliveryToday(idUser);

        [HttpGet("getContactToday/{idUser}")]
        public List<ContactHour> GetContactHourToday(int idUser) => ContactHour.GetContactHoursToday(idUser);

        [HttpGet("getUserById/{idUser}")]
        public Teacher GetUserById(int idUser) => new Teacher().GetUserById(idUser);

        [HttpPut("updatePersonalData/{idUser}")]
        public int UpdatePersonalData(int idUser, [FromBody] Teacher teacherRequest) => Teacher.UpdatePersonalData(idUser, teacherRequest);

        [HttpPut("updateNotification/{idUser}")]
        public int UpdateNotification(int idUser, [FromBody] Teacher teacherResponse) => Teacher.UpdateNotification(idUser, teacherResponse);

        [HttpPut("updateAcademicFormation/{idUser}")]
        public int UpdateAcademicFormation(int idUser, [FromBody] Teacher teacherResponse) => Teacher.UpdateAcademicFormation(idUser, teacherResponse);

        [HttpPut("updateWorkplace/{idUser}")]
        public int UpdateWorkPlace(int idUser, [FromBody] Teacher teacherResponse) => Teacher.UpdateWorkPlace(idUser, teacherResponse);

        [HttpPut("updateUserStatus/{idUser}")]
        public int UpdateUserStatus(int idUser) => Teacher.UpdateUserStatus(idUser);

        [HttpGet("getNotificationStatusById/{idUser}")]
        public int GetNotificationStatusById(int idUser) => Teacher.GetNotificationStatusById(idUser);

        [HttpGet("getNotificationById/{idUser}")]
        public List<Notification> GetNotificationById(int idUser) => Teacher.GetNotificationsById(idUser);

        [HttpGet("getAllSchoolYears")]
        public List<SchoolYear> GetSchoolYears() => SchoolYear.GetAllSchoolYear();

        [HttpGet("getAllEventsCalendar/{idUser}")]
        public List<EventCalendar> GetAllEventsTwoYears(int idUser) => EventCalendar.GetAllEventsTwoYears(idUser);

        [HttpGet("getClassByDay/{idUser}/{date}")]
        public List<Lesson> GetClassByDay(int idUser, string date) => Lesson.GetClassByDay(idUser, date);

        [HttpGet("getContactHourByDay/{idUser}/{date}")]
        public List<ContactHour> GetContactHourByDay(int idUser, string date) => ContactHour.GetContactHourByDay(idUser, date);

        [HttpGet("getJobDeliveryByDay/{idUser}/{date}")]
        public List<JobDelivery> GetJobDeliveryByDay(int idUser, string date) => JobDelivery.GetJobDeliveryByDay(idUser, date);

        [HttpPost("sendEmail")]
        public int SendEmail([FromBody]Email dataEmail) => Email.SendEmail(dataEmail);

        [HttpGet("getPlanification")]
        public List<Discipline> GetPlanificationBy(
            [FromQuery]int idUser,
            [FromQuery] int idSchoolYear,
            [FromQuery] int idDiscipline)
            => Discipline.GetPlanifications(idUser, idSchoolYear, idDiscipline);
        
        [HttpGet("getCourseByUserSchoolYear/{idUser}/{idSchoolYear}")]
        public List<Course> GetCourseByUserSchoolYear(int idUser, int idSchoolYear) => Course.GetCourseByUserSchoolYear(idUser, idSchoolYear);

        [HttpDelete("deleteCourse/{idCourse}")]
        public int DeleteCourse(int idCourse) => Course.DeleteCourse(idCourse);

        [HttpPut("updateCourse/{idCourse}")]
        public int UpdateCourse(int idCourse, [FromBody] Course course) => Course.UpdateCourse(idCourse, course);

        [HttpPost("addCourse")]
        public int AddCourse([FromBody]Discipline discipline) => Course.AddCourse(discipline);

        [HttpGet("getDisciplinesByCourse/{idUser}/{idCourse}")]
        public IEnumerable<Discipline> GetDisciplinesByCourse(int idUser, int idCourse) => Discipline.GetDisciplineByCourse(idUser, idCourse);

        [HttpPost("addDiscipline")]
        public int AddDiscipline([FromBody]Discipline discipline) => Discipline.AddDiscipline(discipline);

        [HttpPut("updateDiscipline/{idDiscipline}")]
        public int UpdateDiscipline(int idDiscipline, [FromBody] Discipline discipline) => Discipline.UpdateDiscipline(idDiscipline, discipline);

        [HttpDelete("deleteDiscipline/{idDiscipline}")]
        public int DeleteDiscipline(int idDiscipline) => Discipline.DeleteDiscipline(idDiscipline);

        [HttpPost("addStudent")]
        public int AddStudent([FromBody]Student student) => Student.AddStudent(student);

        [HttpPost("addStudentDiscipline")]
        public int AddStudentToDiscipline([FromBody]Student student) => Student.AddStudentToDiscipline(student);

        [HttpPut("updateStudent/{idStudent}")]
        public int UpdateStudent(int idStudent, [FromBody] Student student) => Student.UpdateStudent(idStudent, student);

        [HttpDelete("deleteStudent/{idDiscipline}/{idStudent}")]
        public int DeleteStudent(int idDiscipline, int idStudent) => Student.DeleteStudentDiscipline(idDiscipline, idStudent);

        [HttpGet("getStudentByDiscipline/{idDiscipline}")]
        public List<Student> GetStudentByDiscipline(int idDiscipline) => Student.GetStudentByDiscipline(idDiscipline);

        [HttpGet("getStudentInsert/{idUser}")]
        public List<Student> GetStudentInsert(int idUser) => Student.GetStudentInsert(idUser);

        [HttpGet("getTimeAttandace/{idDiscipline}")]
        public string[] GetTimeAttandace(int idDiscipline) => Discipline.GetTimeAttandace(idDiscipline);

        [HttpPost("sendEmailAdministrative")]
        public int SendEmailAdministrative([FromBody]Email dataEmail) => Email.SendEmailAdministrative(dataEmail);

        [HttpDelete("deleteContactHour/{idContactHour}")]
        public int DeleteContactHour(int idContactHour) => ContactHour.DeleteContactHour(idContactHour);

        [HttpGet("getContactHourByDiscipline/{idDiscipline}")]
        public List<ContactHour> GetOntactHourByDiscipline(int idDiscipline) => ContactHour.GetContactHourByDiscipline(idDiscipline);

        [HttpDelete("deleteJobDelivery/{idJobDelivery}")]
        public int DeleteJobDelivery(int idJobDelivery) => JobDelivery.DeleteJobDelivery(idJobDelivery);

        [HttpGet("getJobDeliveryByDiscipline/{idDiscipline}")]
        public List<JobDelivery> GetJobDeliveryByDiscipline(int idDiscipline) => JobDelivery.GetJobDeliveryByDiscipline(idDiscipline);

        [HttpGet("getClassByDiscipline/{idDiscipline}")]
        public List<Lesson> GetClassByDiscipline(int idDiscipline) => Lesson.GetClassByDiscipline(idDiscipline);

        [HttpPost("addJobDelivery")]
        public int AddJobDelivery([FromBody]JobDelivery jobDelivery) => JobDelivery.AddJobDelivery(jobDelivery);

        [HttpPost("addContactHour")]
        public int AddCotactHour([FromBody]ContactHour contactHour) => ContactHour.AddContactHour(contactHour);

        [HttpPut("updateJobDelivery/{idJobDelivery}")]
        public int UpdateJobDelivery(int idJobDelivery, [FromBody]JobDelivery jobDelivery) => JobDelivery.UpdateJobDelivery(idJobDelivery, jobDelivery);

        [HttpPut("updateContactHour/{idContactHour}")]
        public int UpdateContactHour(int idContactHour, [FromBody]ContactHour contactHour) => ContactHour.UpdateContactHour(idContactHour, contactHour);

        [HttpGet("getCurrentLessonNumberForDiscipline/{idDiscipline}")]
        public int GetCurrentLessonNumberForDiscipline(int idDiscipline) => Lesson.GetCurrentClassNumberForDiscipline(idDiscipline);

        [HttpDelete("deleteClass/{idClass}")]
        public int DeleteClass(int idClass) => Lesson.DeleteClass(idClass);

        [HttpPost("addClass")]
        public int AddClass([FromBody]Lesson lesson) => Lesson.AddLesson(lesson);

        [HttpPut("updateClass/{idClass}")]
        public int UpdateClass(int idClass, [FromBody]Lesson lesson) => Lesson.UpdateLesson(idClass, lesson);
         
        [HttpPut("copyDiscipline/{idDisciplineFrom}/{idDisciplineTo}")]
        public int CopyDiscipline(int idDisciplineFrom, int idDisciplineTo) => Discipline.CopyDiscipline(idDisciplineFrom, idDisciplineTo);

        [HttpGet("getContentsByClass/{idClass}")]
        public List<LessonContent> GetContentsByClass(int idClass) => Lesson.GetClassContent(idClass);

        [HttpPost("addClassContent")]
        public int AddClassContent([FromBody]LessonContent lessonContent) => LessonContent.AddContent(lessonContent);

        [HttpDelete("deleteClassContent/{idClassContent}")]
        public int DeleteClassContent(int idClassContent) => LessonContent.DeleteContent(idClassContent);

        [HttpGet("getMaterialsByClass/{idClass}")]
        public List<LessonMaterial> GetMaterialsByClass(int idClass) => Lesson.GetClassMaterial(idClass);

        [HttpPost("addClassMaterial")]
        public int AddClassMaterial([FromBody]LessonMaterial lessonMaterial) => LessonMaterial.AddMaterial(lessonMaterial);

        [HttpDelete("deleteClassMaterial/{idClassMaterial}")]
        public int DeleteClassMaterial(int idClassMaterial) => LessonMaterial.DeleteMaterial(idClassMaterial);
    }
}