﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class JobDelivery
    {
        #region CONST

        public JobDelivery()
        {
            this.Id = 0;
            this.Discipline = "";
            this.Course = "";
            this.Description = "";
            this.Day = "";
            this.MaxTime = "00:00";
            this.DeliveryLocation = "";
            this.IdDiscipline = -1;

        }

        public JobDelivery(int id, string discipline, string course, string description,  string day, string maxTime,  string deliveryLocation, int idDiscipline)
        {
            this.Id = id;
            this.Discipline = discipline;
            this.Course = course;
            this.Description = description;
            this.Day = day;
            this.MaxTime = maxTime;
            this.DeliveryLocation = deliveryLocation;
            this.IdDiscipline = idDiscipline;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Discipline { get; set; }

        public string Course { get; set; }

        public string Description { get; set; }

        public string Day { get; set; }

        public string MaxTime { get; set; }

        public string DeliveryLocation { get; set; }

        public int IdDiscipline { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que devolve as entregas de um dia
        /// </summary>
        public static List<JobDelivery> GetJobDeliveryToday(int idUser)
        {
            var jobs = new List<JobDelivery>();
            var con = OpenConnection();

            try
            {
                const string query = "SELECT j.idJ, j.descriptionJ, j.dateJ, j.maxTimeJ, j.deliveryLocationJ, d.nameDis, cou.nameC FROM jobdelivery j INNER JOIN discipline d ON j.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND j.dateJ = date(now()) AND j.maxTimeJ > now() ORDER BY j.maxTimeJ ASC;";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var job = new JobDelivery();
                        var auxDate = new DateTime();
                        var auxTime = new DateTime();

                        job.Id = dataReader.GetInt32("idJ");
                        job.Description = dataReader.GetString("descriptionJ");
                        job.Course = dataReader.GetString("nameC");

                        auxDate  = dataReader.GetDateTime("dateJ");
                        job.Day = auxDate.ToString("dd/MM/yyyy");

                        auxTime = dataReader.GetDateTime("maxTimeJ");
                        job.MaxTime = auxTime.ToString("HH:mm");

                        job.DeliveryLocation = dataReader.GetString("deliveryLocationJ");
                        job.Discipline = dataReader.GetString("nameDis");

                        jobs.Add(job);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return jobs;
        }

        /// <summary>
        /// Método que devolve as entregas de um dia
        /// </summary>
        public static List<JobDelivery> GetJobDeliveryByDiscipline(int idDiscipline)
        {
            var jobs = new List<JobDelivery>();
            var con = OpenConnection();

            try
            {
                const string query = "SELECT j.idJ, j.descriptionJ, j.dateJ, j.maxTimeJ, j.deliveryLocationJ, d.nameDis, cou.nameC FROM jobdelivery j INNER JOIN discipline d ON j.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT WHERE d.idDis = @idDiscipline ORDER BY j.maxTimeJ ASC;";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows )
                {
                    while (dataReader.Read())
                    {
                        var job = new JobDelivery();
                        var auxDate = new DateTime();
                        var auxTime = new DateTime();

                        job.Id = dataReader.GetInt32("idJ");
                        job.Description = dataReader.GetString("descriptionJ");
                        job.Course = dataReader.GetString("nameC");

                        auxDate = dataReader.GetDateTime("dateJ");
                        job.Day = auxDate.ToString("dd/MM/yyyy");

                        auxTime = dataReader.GetDateTime("maxTimeJ");
                        job.MaxTime = auxTime.ToString("HH:mm");

                        job.DeliveryLocation = dataReader.GetString("deliveryLocationJ");
                        job.Discipline = dataReader.GetString("nameDis");

                        jobs.Add(job);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return jobs;
        }

        /// <summary>
        /// Método que devolve as notificaçoes das entregas que terão inicio brevemente
        /// </summary>
        public static IEnumerable<Notification> GetJobDeliveryForNotification(int idUser)
        {
            var notifications = new List<Notification>();
            var con = OpenConnection();
            const int pre = 2;

            try
            {
                const string query = "SELECT j.idJ, j.maxTimeJ, j.deliveryLocationJ, j.dateJ, d.nameDis FROM jobdelivery j INNER JOIN discipline d ON j.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND j.maxTimeJ BETWEEN now() AND DATE_ADD(now(), INTERVAL 1 HOUR);";

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var notification = new Notification();
                        var auxTime = new DateTime();
                        var auxDate = new DateTime();

                        notification.Id = dataReader.GetInt32("idJ");
                        notification.Id = int.Parse(pre.ToString() + notification.Id);

                        auxDate = dataReader.GetDateTime("dateJ");
                        notification.Day = auxDate.ToString("dd/MM/yyyy");

                        auxTime = dataReader.GetDateTime("maxTimeJ");
                        notification.Hour = auxTime.ToString("HH:mm");

                        notification.Discipline = dataReader.GetString("nameDis");
                        notification.RoomOrMethod = dataReader.GetString("deliveryLocationJ");

                        notification.Title = "Entrega";

                        notifications.Add(notification);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return notifications;
        }

        /// <summary>
        /// Método que devolde todos os dias em que tem aulas marcadas no ultimo e proximo ano
        /// </summary>
        public static IEnumerable<DateTime> GetJobDeliveryTwoYear(int idUser)
        {
            var jobs = new List<DateTime>();
            var con = OpenConnection();

            const string query = "SELECT DISTINCT(j.dateJ) FROM jobdelivery j INNER JOIN discipline d ON j.idDis = d.idDis INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND j.maxTimeJ BETWEEN DATE_ADD(DATE(now()), INTERVAL -1 Year) AND DATE_ADD(DATE(now()), INTERVAL 1 Year);";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var auxTime = dataReader.GetDateTime("dateJ");

                        jobs.Add(auxTime);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return jobs;
        }

        /// <summary>
        /// Método que devolve os apoios de um determinado dia
        /// </summary>
        public static List<JobDelivery> GetJobDeliveryByDay(int idUser, string date)
        {
            var jobs = new List<JobDelivery>();
            var con = OpenConnection();

            const string query = "SELECT j.idJ, j.descriptionJ, j.dateJ, j.maxTimeJ, j.deliveryLocationJ, d.nameDis, cou.nameC FROM jobdelivery j INNER JOIN discipline d ON j.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND j.dateJ = @date ORDER BY j.maxTimeJ ASC;";

            try
            {
                var day = DateTime.Parse(date);

                var cmdSelect = new MySqlCommand(query, con);

                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@date", day);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var job = new JobDelivery
                        {
                            Id = dataReader.GetInt32("idJ"),
                            Description = dataReader.GetString("descriptionJ"),
                            Course = dataReader.GetString("nameC")
                        };


                        var auxDate = dataReader.GetDateTime("dateJ");
                        job.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("maxTimeJ");
                        job.MaxTime = auxTime.ToString("HH:mm");

                        job.DeliveryLocation = dataReader.GetString("deliveryLocationJ");
                        job.Discipline = dataReader.GetString("nameDis");

                        jobs.Add(job);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return jobs;
        }

        /// <summary>
        /// Método que premite eliminar uma entrega
        /// </summary>
        public static int DeleteJobDelivery(int idJob)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM jobDelivery WHERE idJ = @job;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@job", idJob);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite adicionar uma nova entrega
        /// </summary>
        public static int AddJobDelivery(JobDelivery job)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "INSERT INTO jobDelivery (idDis, descriptionJ, dateJ, maxTimeJ, deliveryLocationJ, notificationIssueJ) " +
                "VALUES (@idDiscipline, @description, @date, @maxTime, @location, 0);";

            try
            {

                var cmdInsert = new MySqlCommand(query, con);

                 cmdInsert.Parameters.AddWithValue("@idDiscipline", job.IdDiscipline);
                 cmdInsert.Parameters.AddWithValue("@description", job.Description);

                 string[] splitDate = job.Day.Split("/");
                 string[] splitHour = job.MaxTime.Split(":");

                 cmdInsert.Parameters.AddWithValue("@date", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                 cmdInsert.Parameters.AddWithValue("@maxTime", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                 cmdInsert.Parameters.AddWithValue("@location", job.DeliveryLocation); 

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite atualizar os dados de uma determinada entrega
        /// /// </summary>
        public static int UpdateJobDelivery(int idJobDelivery, JobDelivery jobDelivery)
        {
            var result = -1;

            var con = OpenConnection();
            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE jobdelivery SET descriptionJ = @description, dateJ = @date, maxTimeJ = @maxTime, deliveryLocationJ = @location WHERE idJ = @idJobDelivery; "
                };

                string[] splitDate = jobDelivery.Day.Split("/");
                string[] splitHour = jobDelivery.MaxTime.Split(":");

                cmdUpdate.Parameters.AddWithValue("@date", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                cmdUpdate.Parameters.AddWithValue("@maxTime", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                 cmdUpdate.Parameters.AddWithValue("@description", jobDelivery.Description);
                 cmdUpdate.Parameters.AddWithValue("@location", jobDelivery.DeliveryLocation);
                 cmdUpdate.Parameters.AddWithValue("@idJobDelivery", idJobDelivery);

                result = cmdUpdate.ExecuteNonQuery();


           }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            { 
                CloseConnection(con);
            }

            return result;

        }
        #endregion
    }
}
