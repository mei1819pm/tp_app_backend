﻿namespace TP_AppBackEnd.Model
{
    public class Notification
    {
        #region CONST

        public Notification()
        {
            this.Id = -1;
            this.Title = "";
            this.Discipline = "";
            this.Hour = "";
            this.RoomOrMethod = "";
            this.Student = "";
            this.Day = "";
        }

        public Notification(int id, string title, string discipline, string hour, string roomOrMethod, string student, string day)
        {
            this.Id = -1;
            this.Title = title;
            this.Discipline = discipline;
            this.Hour = hour;
            this.RoomOrMethod = roomOrMethod;
            this.Student = student;
            this.Day = day;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Title { get; set; }

        public string Discipline { get; set; }

        public string Hour { get; set; }

        public string RoomOrMethod { get; set; }

        public string Student { get; set; }

        public string Day { get; set; }

        #endregion

    }
}
