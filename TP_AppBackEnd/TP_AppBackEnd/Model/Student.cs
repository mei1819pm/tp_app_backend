﻿using System;
using System.Collections.Generic;
using static TP_AppBackEnd.DataBase.DataBase;
using MySql.Data.MySqlClient;

namespace TP_AppBackEnd.Model
{
    public class Student
    {

        #region CONST

        public Student()
        {
            this.Id = 0;
            this.Number = -1;
            this.Name = "";
            this.Email = "";
            this.Password = "0000";
            this.Delegate = 0;
            this.IdDiscipline = -1;
            this.IdTeacher = -1;
        }

        public Student(int id, int nmber, string name, string email, int del, int idDiscipline, int idTeacher)
        {
            this.Id = id;
            this.Number = nmber;
            this.Name = name;
            this.Email = email;
            this.Password = "0000";
            this.Delegate = del;
            this.IdDiscipline = idDiscipline;
            this.IdTeacher = idTeacher;

        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public int Number { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int Delegate { get; set; }

        public int IdDiscipline { get; set; }

        public int IdTeacher { get; set; }

        #endregion

        #region OTHER


        /// <summary>
        /// Método que devolve os estudantes que estao associados a uma determinada disciplina
        /// </summary>
        public static List<Student> GetStudentByDiscipline(int idDiscipline)
        {
            var students = new List<Student>();
            var con = OpenConnection();

            const string query = "SELECT s.idS, s.numberS, s.nameS, s.emailS, ds.delegateDS FROM student s INNER JOIN disciplinestudente ds ON s.idS = ds.idS WHERE ds.idDis = @idDiscipline; ";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var student = new Student
                        {

                            Id = dataReader.GetInt32("idS"),
                            Number = dataReader.GetInt32("numberS"),
                            Name = dataReader.GetString("nameS"),
                            Email = dataReader.GetString("emailS"),
                            Delegate = dataReader.GetInt32("delegateDS")
                        };

                        students.Add(student);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return students;
        }

        /// <summary>
        /// Método que devolve todos os estudantes inseridos por um professor
        /// </summary>
        public static List<Student> GetStudentInsert(int idUser)
        {
            var students = new List<Student>();
            var con = OpenConnection();

            const string query = "SELECT s.idS, s.numberS, s.nameS FROM student s WHERE s.idT = @idUser; ";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var student = new Student
                        {

                            Id = dataReader.GetInt32("idS"),
                            Number = dataReader.GetInt32("numberS"),
                            Name = dataReader.GetString("nameS"),
                        };

                        students.Add(student);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return students;
        }

        /// <summary>
        /// Método que premite eliminar um estudante de uma disciplina
        /// </summary>
        public static int DeleteStudentDiscipline(int idDiscipline, int idStudent)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM disciplinestudente WHERE idDis = @discipline AND idS = @student;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@discipline", idDiscipline);
                cmdDelete.Parameters.AddWithValue("@student", idStudent);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite adicionar um novo aluno
        /// </summary>
        public static int AddStudent(Student student)
        {
            int result = -1;
            int auxResult;

            var con = OpenConnection();

            try
            {
                const string query = "INSERT INTO student(numberS, nameS, emailS, passwordS, idT) VALUES(@number, @name, @email, @password, @idTeacher); ";

                var cmdInsert = new MySqlCommand(query, con);
                cmdInsert.Parameters.AddWithValue("@number", student.Number);
                cmdInsert.Parameters.AddWithValue("@name", student.Name);
                cmdInsert.Parameters.AddWithValue("@email", student.Email);
                cmdInsert.Parameters.AddWithValue("@password", "0000");
                cmdInsert.Parameters.AddWithValue("@idTeacher", student.IdTeacher);

                auxResult = cmdInsert.ExecuteNonQuery();

                if (auxResult > 0)
                {
                    student.Id = LastId();
                }

                if (student.Id > 0)
                {
                    result = AddStudentToDiscipline(student);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite associar um estudante a uma disciplina
        /// </summary>
        public static int AddStudentToDiscipline(Student student)
        {
            int result = -1;

            var con = OpenConnection();

            try
            {
                const string query = "INSERT INTO disciplinestudente VALUES(@discipline, @student, @delegate); ";

                var cmdInsert = new MySqlCommand(query, con);
                cmdInsert.Parameters.AddWithValue("@discipline", student.IdDiscipline);
                cmdInsert.Parameters.AddWithValue("@student", student.Id);
                cmdInsert.Parameters.AddWithValue("@delegate", student.Delegate);

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite atualizar estudante e sua associação a uma disciplina
        /// </summary>
        public static int UpdateStudent(int idStudent, Student student)
        {
            var result = -1;

            var con = OpenConnection();
            try
            {
                var cmdUpdateStudent = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE student SET numberS = @number, nameS = @name, emailS = @email WHERE idS = @student; "
                };
                cmdUpdateStudent.Parameters.AddWithValue("@number", student.Number);
                cmdUpdateStudent.Parameters.AddWithValue("@name", student.Name);
                cmdUpdateStudent.Parameters.AddWithValue("@email", student.Email);
                cmdUpdateStudent.Parameters.AddWithValue("@student", idStudent);

                var cmdUpdateStudentDiscipline = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                    "UPDATE disciplinestudente SET delegateDS = @delegate WHERE idS = @student AND idDis = @discipline; "
                };
                cmdUpdateStudentDiscipline.Parameters.AddWithValue("@delegate", student.Delegate);
                cmdUpdateStudentDiscipline.Parameters.AddWithValue("@discipline", student.IdDiscipline);
                cmdUpdateStudentDiscipline.Parameters.AddWithValue("@student", idStudent);

                result = cmdUpdateStudent.ExecuteNonQuery();
                if (result > 0)
                {
                    result = cmdUpdateStudentDiscipline.ExecuteNonQuery();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;

        }

        public static int LastId()
        {
            int result = -1;
            var con = OpenConnection();

            const string query = "SELECT MAX(idS) FROM student;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    result = dataReader.GetInt32("MAX(idS)");
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        #endregion

    }
}
