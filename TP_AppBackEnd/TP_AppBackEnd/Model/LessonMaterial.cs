﻿using MySql.Data.MySqlClient;
using System;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class LessonMaterial
    {

        #region CONST

        public LessonMaterial()
        {
            this.Id = 0;
            this.Description = "";


        }

        public LessonMaterial(int id, string description)
        {
            this.Id = id;
            this.Description = description;

        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Description { get; set; }

        public int IdClass { get; set; }
        #endregion

        #region OTHER

        public static int AddMaterial(LessonMaterial content)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "INSERT INTO classmaterial (descriptionCM, idCl) " +
                "VALUES (@description, @idClass);";

            try
            {

                var cmdInsert = new MySqlCommand(query, con);

                cmdInsert.Parameters.AddWithValue("@idClass", content.IdClass);
                cmdInsert.Parameters.AddWithValue("@description", content.Description);

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static int DeleteMaterial(int id)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM classmaterial WHERE idCM = @id;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@id", id);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }
        #endregion
    }
}
