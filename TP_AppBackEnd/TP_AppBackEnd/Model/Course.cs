﻿using System;
using System.Collections.Generic;
using static TP_AppBackEnd.DataBase.DataBase;
using MySql.Data.MySqlClient;

namespace TP_AppBackEnd.Model
{
    public class Course
    {

        #region CONST

        public Course()
        {
            this.Id = 0;
            this.Description = "";
            this.Semester = "";
        }

        public Course(int id, string description, string semester)
        {
            this.Id = id;
            this.Description = description;
            this.Semester = semester;

        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Description { get; set; }

        public string Semester { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que devolve os cursos que um determinado professor leciona num determinado ano letivo
        /// </summary>
        public static List<Course> GetCourseByUserSchoolYear(int idUser, int idSchoolYear)
        {
            var courses = new List<Course>();
            var con = OpenConnection();

            const string query = "SELECT c.idC, c.nameC, s.descriptionS FROM teacher t INNER JOIN discipline d ON t.idT = d.idT INNER JOIN course c ON d.idC = c.idC INNER JOIN schoolyear sy ON d.idSY = sy.idSY INNER JOIN semester s ON d.idS = s.idS WHERE t.idT = @idUser AND sy.idSY = @idSchoolYear ORDER BY s.numberS ASC; ";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@idSchoolYear", idSchoolYear);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var course = new Course
                        {

                            Id = dataReader.GetInt32("idC"),
                            Description = dataReader.GetString("nameC"),
                            Semester = dataReader.GetString("descriptionS")
                        };

                        courses.Add(course);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return courses;
        }

        /// <summary>
        /// Método que premite eliminar um curso
        /// </summary>
        public static int DeleteCourse(int idCourse)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM course WHERE idC= @idCourse; ";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@idCourse", idCourse);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite atualizar o nome de um determinado curso
        /// </summary>
        public static int UpdateCourse(int idCourse, Course course)
        {
            var result = -1;

            var con = OpenConnection();
            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE course c SET c.nameC=@description  WHERE c.idC = @idCourse;"
                };
                cmdUpdate.Parameters.AddWithValue("@idCourse", idCourse);
                cmdUpdate.Parameters.AddWithValue("@description", course.Description);
                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;

        }

        public static int AddCourse(Discipline discipline)
        {
            int result = -1;
            int auxResult;

            var con = OpenConnection();

            try
            {
                const string query = "INSERT INTO course (nameC) VALUES (@name);";

                var cmdInsert = new MySqlCommand(query, con);
                cmdInsert.Parameters.AddWithValue("@name", discipline.NameCourse);

                auxResult = cmdInsert.ExecuteNonQuery();

                if(auxResult > 0)
                {
                    discipline.IdCourse = LastId();
                }

                if (discipline.IdCourse > 0)
                {
                   result = Discipline.AddDiscipline(discipline);
                }
             
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static int LastId()
        {
            int result = -1;
            var con = OpenConnection();

            const string query = "SELECT MAX(idC) FROM course;;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
              
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    result = dataReader.GetInt32("MAX(idC)");
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        #endregion

    }
}
