﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class Lesson
    {
        #region CONST

        public Lesson()
        {
            this.Id = 0;
            this.Discipline = "";
            this.Course = "";
            this.Number = 0;
            this.Day = "";
            this.Start = "00:00";
            this.End = "00:00";
            this.ClassRoom = "";
            this.TypeClass = "";
            this.ClassContent = new List<LessonContent>();
            this.ClassMaterial = new List<LessonMaterial>();

        }

        public Lesson(int id, string discipline, string course, int number, string day, string start, string end, string classRoom, string typeClass, List<LessonContent> classContent, List<LessonMaterial> classMaterial)
        {
            this.Id = id;
            this.Discipline = discipline;
            this.Course = course;
            this.Number = number;
            this.Day = day;
            this.Start = start;
            this.End = end;
            this.ClassRoom = classRoom;
            this.TypeClass = typeClass;
            this.ClassContent = classContent;
            this.ClassMaterial = classMaterial;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public int IdDiscipline { get; set; }

        public string Discipline { get; set; }

        public string Course { get; set; }

        public int Number { get; set; }

        public string Day { get; set; }

        public string Start { get; set; }

        public string End { get; set; }

        public string ClassRoom { get; set; }

        public string TypeClass { get; set; }

        public List<LessonContent> ClassContent { get; set; }

        public List<LessonMaterial> ClassMaterial { get; set; }

        #endregion

        #region OTHERS

        public static List<Lesson> GetClassToday(int idUser)
        {
            var classes = new List<Lesson>();
            var con = OpenConnection();

            const string query = "SELECT c.idCl, c.numberCl, c.dayCl, c.startTimeCl, c.endTimeCl, c.classroomCl, c.typeCl, d.nameDis, cou.nameC FROM class c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC  INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND c.dayCl = date(now()) AND c.endTimeCl > now() ORDER BY c.startTimeCl ASC;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var lessonToday = new Lesson();

                        lessonToday.Id = dataReader.GetInt32("idCl");
                        lessonToday.Course = dataReader.GetString("nameC");
                        lessonToday.Number = dataReader.GetInt32("numberCl");
                        lessonToday.ClassRoom = dataReader.GetString("classroomCl");
                        lessonToday.TypeClass = dataReader.GetString("typeCl");
                        lessonToday.Discipline = dataReader.GetString("nameDis");


                        lessonToday.ClassContent.AddRange(GetClassContent(lessonToday.Id));
                        lessonToday.ClassMaterial.AddRange(GetClassMaterial(lessonToday.Id));

                        var auxDate = dataReader.GetDateTime("dayCl");
                        lessonToday.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("startTimeCl");
                        lessonToday.Start = auxTime.ToString("HH:mm");

                        auxTime = dataReader.GetDateTime("endTimeCl");
                        lessonToday.End = auxTime.ToString("HH:mm");

                        classes.Add(lessonToday);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classes;
        }

        public static List<Lesson> GetClassByDiscipline(int idDiscipline)
        {
            var classes = new List<Lesson>();
            var con = OpenConnection();

            const string query = "SELECT c.idCl, c.numberCl, c.dayCl, c.startTimeCl, c.endTimeCl, c.classroomCl, c.typeCl FROM class c INNER JOIN discipline d ON c.idDis = d.idDis  WHERE d.idDis = @idDiscipline ORDER BY c.startTimeCl ASC;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var lessonToday = new Lesson
                        {
                            Id = dataReader.GetInt32("idCl"),
                            Number = dataReader.GetInt32("numberCl"),
                            ClassRoom = dataReader.GetString("classroomCl"),
                            TypeClass = dataReader.GetString("typeCl")
                        };


                        lessonToday.ClassContent.AddRange(GetClassContent(lessonToday.Id));
                        lessonToday.ClassMaterial.AddRange(GetClassMaterial(lessonToday.Id));

                        var auxDate = dataReader.GetDateTime("dayCl");
                        lessonToday.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("startTimeCl");
                        lessonToday.Start = auxTime.ToString("HH:mm");

                        auxTime = dataReader.GetDateTime("endTimeCl");
                        lessonToday.End = auxTime.ToString("HH:mm");

                        classes.Add(lessonToday);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classes;
        }

        public static List<Lesson> GetClassById(int id)
        {
            var classes = new List<Lesson>();
            var con = OpenConnection();

            const string query = @"SELECT c.idCl, c.numberCl, c.dayCl, c.startTimeCl, c.endTimeCl, c.classroomCl, c.typeCl FROM class c 
                                    INNER JOIN discipline d ON c.idDis = d.idDis  
                                    WHERE c.idCl = @id;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@id", id);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var lessonToday = new Lesson
                        {
                            Id = dataReader.GetInt32("idCl"),
                            Number = dataReader.GetInt32("numberCl"),
                            ClassRoom = dataReader.GetString("classroomCl"),
                            TypeClass = dataReader.GetString("typeCl")
                        };


                        lessonToday.ClassContent.AddRange(GetClassContent(lessonToday.Id));
                        lessonToday.ClassMaterial.AddRange(GetClassMaterial(lessonToday.Id));

                        var auxDate = dataReader.GetDateTime("dayCl");
                        lessonToday.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("startTimeCl");
                        lessonToday.Start = auxTime.ToString("HH:mm");

                        auxTime = dataReader.GetDateTime("endTimeCl");
                        lessonToday.End = auxTime.ToString("HH:mm");

                        classes.Add(lessonToday);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classes;
        }

        /// <summary>
        /// Método que devolve as notificaçoes das aulas que terão inicio brevemente
        /// </summary>
        public static IEnumerable<Notification> GetClassForNotification(int idUser)
        {
            var notifications = new List<Notification>();
            var con = OpenConnection();

            const int pre = 1;
            const string query = "SELECT c.idCl, c.startTimeCl, c.classroomCl, c.dayCl, d.nameDis FROM class c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC  INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND c.startTimeCl BETWEEN now() AND DATE_ADD(now(), INTERVAL 1 HOUR);";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var notification = new Notification
                        {
                            Discipline = dataReader.GetString("nameDis"),
                            RoomOrMethod = dataReader.GetString("classroomCl"),
                            Title = "Aula",
                            Id = dataReader.GetInt32("idCl")
                        };

                        notification.Id = int.Parse(pre.ToString() + notification.Id);

                        var auxDate = dataReader.GetDateTime("dayCl");
                        notification.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("startTimeCl");
                        notification.Hour = auxTime.ToString("HH:mm");

                        notifications.Add(notification);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return notifications;
        }

        /// <summary>
        /// Método que devolde todos os dias em que tem aulas marcadas no ultimo e proximo ano
        /// </summary>
        public static IEnumerable<DateTime> GetClassTwoYear(int idUser)
        {

            var classes = new List<DateTime>();
            var con = OpenConnection();

            const string query = "SELECT DISTINCT(c.dayCl) FROM class c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND c.dayCl BETWEEN DATE_ADD(DATE(now()), INTERVAL -1 Year) AND DATE_ADD(DATE(now()), INTERVAL 1 Year) ;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var auxTime = dataReader.GetDateTime("dayCl");

                        classes.Add(auxTime);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classes;
        }

        public static List<Lesson> GetClassByDay(int idUser, string date)
        {
            var classes = new List<Lesson>();
            var con = OpenConnection();

            const string query = "SELECT c.idCl, c.numberCl, c.dayCl, c.startTimeCl, c.endTimeCl, c.classroomCl, c.typeCl, d.nameDis, cou.nameC FROM class c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC  INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND c.dayCl = @date ORDER BY c.startTimeCl ASC;";

            try
            {
                var time = DateTime.Parse(date);

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@date", time);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var lessonToday = new Lesson
                        {
                            Id = dataReader.GetInt32("idCl"),
                            Course = dataReader.GetString("nameC"),
                            Number = dataReader.GetInt32("numberCl"),
                            ClassRoom = dataReader.GetString("classroomCl"),
                            TypeClass = dataReader.GetString("typeCl"),
                            Discipline = dataReader.GetString("nameDis")
                        };

                        lessonToday.ClassContent.AddRange(GetClassContent(lessonToday.Id));
                        lessonToday.ClassMaterial.AddRange(GetClassMaterial(lessonToday.Id));

                        var auxDate = dataReader.GetDateTime("dayCl");
                        lessonToday.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("startTimeCl");
                        lessonToday.Start = auxTime.ToString("HH:mm");

                        auxTime = dataReader.GetDateTime("endTimeCl");
                        lessonToday.End = auxTime.ToString("HH:mm");

                        classes.Add(lessonToday);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classes;
        }

        /// <summary>
        /// Método que premite eliminar uma hora de contacto
        /// </summary>
        public static int DeleteClass(int idClass)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM class WHERE idCl = @idClass;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@idClass", idClass);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite devolver o conteudo de uma determinada aula
        /// </summary>
        public static List<LessonContent> GetClassContent(int idClass)
        {
            var classContent = new List<LessonContent>();
            var con = OpenConnection();

            const string query = "SELECT c.idCC, c.descriptionCC FROM classcontent c WHERE c.idCl = @idClass";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idClass", idClass);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var content = new LessonContent();
                        content.Id = dataReader.GetInt32("idCC");
                        content.Description = dataReader.GetString("descriptionCC");
                        content.IdClass = idClass;
                        classContent.Add(content);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classContent;
        }

        /// <summary>
        /// Método que devolve o material a fornecer de uma determinada aula
        /// </summary>
        public static List<LessonMaterial> GetClassMaterial(int idClass)
        {
            var classMaterial = new List<LessonMaterial>();
            var con = OpenConnection();

            const string query = "SELECT m.idCM, m.descriptionCM FROM classmaterial m WHERE m.idCl = @idClass";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idClass", idClass);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var material = new LessonMaterial();
                        material.Id = dataReader.GetInt32("idCM");
                        material.Description = dataReader.GetString("descriptionCM");
                        material.IdClass = idClass;
                        classMaterial.Add(material);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return classMaterial;
        }

        public static int GetCurrentClassNumberForDiscipline(int idDiscipline)
        {
            int result = 1;
            var con = OpenConnection();

            const string query = "select max(numberCl) as num from class where idDis = @idDiscipline";
            try
            {

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);
                var dataReader = cmdSelect.ExecuteReader();

                dataReader.Read();

                var numberOfClasses = dataReader.GetInt32("num");

                dataReader.Close();

                result = numberOfClasses + 1;
            }
            catch (Exception ex)
            {
                return result;
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static int AddLesson(Lesson lesson)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = @"Insert into class (idDis, numberCl, dayCl, startTimeCl, endTimeCl, classroomCl, typeCl, notificationIssueCl)
                                    values(@idDiscipline, @number, @day, @start, @end, @room, @type, @noti);";

            try
            {

                var cmdInsert = new MySqlCommand(query, con);
                cmdInsert.Parameters.AddWithValue("@idDiscipline", lesson.IdDiscipline);
                cmdInsert.Parameters.AddWithValue("@number", lesson.Number);
                cmdInsert.Parameters.AddWithValue("@room", lesson.ClassRoom);
                cmdInsert.Parameters.AddWithValue("@type", lesson.TypeClass);
                cmdInsert.Parameters.AddWithValue("@noti", 0);


                string[] splitDate = lesson.Day.Split("/");
                string[] splitHourStart = lesson.Start.Split(":");
                string[] splitHourEnd = lesson.End.Split(":");

                cmdInsert.Parameters.AddWithValue("@day", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourStart[0]), int.Parse(splitHourStart[1]), 00));
                cmdInsert.Parameters.AddWithValue("@start", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourStart[0]), int.Parse(splitHourStart[1]), 00));
                cmdInsert.Parameters.AddWithValue("@end", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourEnd[0]), int.Parse(splitHourEnd[1]), 00));

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static int UpdateLesson(int idClass, Lesson lesson)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = @"Update class c
                                   set numberCl = @number, dayCl = @day, startTimeCl = @start, endTimeCl = @end, classroomCl = @room, typeCl = @type, notificationIssueCl = @noti
                                   Where idCl = @id";

            try
            {

                var cmdUpdate = new MySqlCommand(query, con);
                cmdUpdate.Parameters.AddWithValue("@id", idClass);
                cmdUpdate.Parameters.AddWithValue("@number", lesson.Number);
                cmdUpdate.Parameters.AddWithValue("@room", lesson.ClassRoom);
                cmdUpdate.Parameters.AddWithValue("@type", lesson.TypeClass);
                cmdUpdate.Parameters.AddWithValue("@noti", 0); 

                string[] splitDate = lesson.Day.Split("/");
                string[] splitHourStart = lesson.Start.Split(":");
                string[] splitHourEnd = lesson.End.Split(":");

                cmdUpdate.Parameters.AddWithValue("@day", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourStart[0]), int.Parse(splitHourStart[1]), 00));
                cmdUpdate.Parameters.AddWithValue("@start", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourStart[0]), int.Parse(splitHourStart[1]), 00));
                cmdUpdate.Parameters.AddWithValue("@end", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHourEnd[0]), int.Parse(splitHourEnd[1]), 00));

                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            { 
                CloseConnection(con);
            }

            return result;
        }

        public static int LastId()
        {
            int result = -1;
            var con = OpenConnection();

            const string query = "SELECT MAX(idCl) FROM class;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    result = dataReader.GetInt32("MAX(idCl)");
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }
        #endregion
    }
}
