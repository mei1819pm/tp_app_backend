﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class Email
    {
        #region CONST

        public Email()
        {
            this.Id = -1;
            this.IdDiscipline = -1;
            this.EmailFrom = "";
            this.Password = "";
            this.SendTo = -1;
            this.EmailAdministrative = "";
            this.Subject = "";
            this.Body = "";
        }

        public Email(int id, int idDiscipline, string email, string password, int sendTo, string emailAdministrative, string subject, string body)
        {
            this.Id = id;
            this.IdDiscipline = idDiscipline;
            this.EmailFrom = email;
            this.Password = password;
            this.SendTo = sendTo;
            this.EmailAdministrative = emailAdministrative;
            this.Subject = subject;
            this.Body = body;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public int IdDiscipline { get; set; }

        public string EmailFrom { get; set; }

        public string Password { get; set; }

        public int SendTo { get; set; }

        public string EmailAdministrative { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que permite enviar emails aos alunos 
        /// </summary>
        public static int SendEmail(Email dataEmail)
        {
            var result = -1;
            var listEmails = new List<string>();

            var mail = new MailMessage();
            var client = new SmtpClient();

            try
            {

                if (!dataEmail.EmailAdministrative.Equals(""))
                {
                    listEmails.Add(dataEmail.EmailAdministrative);
                }

                var dataTeacher = GetEmailAndPasswordTeacher(dataEmail.Id);
                dataEmail.EmailFrom = dataTeacher[0];
                dataEmail.Password = dataTeacher[1];

                listEmails.AddRange(GetEmailsToSend(dataEmail.IdDiscipline, dataEmail.SendTo));

                mail.From = new MailAddress(dataEmail.EmailFrom);

                foreach (var aux in listEmails)
                {
                    mail.To.Add(aux);
                }
                mail.Subject = dataEmail.Subject;
                mail.Body = dataEmail.Body;

                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(dataEmail.EmailFrom, dataEmail.Password);
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;

                client.Send(mail);
                result = 1;

            }
            catch( Exception ex)
            {
                throw  new Exception(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Método que permite enviar emails ao pessoal administrativo 
        /// </summary>
        public static int SendEmailAdministrative(Email dataEmail)
        {
            var result = -1;
            var listEmails = new List<string>();

            var mail = new MailMessage();
            var client = new SmtpClient();

            try
            {

                var dataTeacher = GetEmailAndPasswordTeacher(dataEmail.Id);
                dataEmail.EmailFrom = dataTeacher[0];
                dataEmail.Password = dataTeacher[1];

                mail.From = new MailAddress(dataEmail.EmailFrom);


                mail.To.Add(dataEmail.EmailAdministrative);

                mail.Subject = dataEmail.Subject;
                mail.Body = dataEmail.Body;

                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(dataEmail.EmailFrom, dataEmail.Password);
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;

                client.Send(mail);
                result = 1;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Método que devolve os emails de todos os alunos ou apenas dos elagados associados a uma disciplina
        /// </summary>
        private static IEnumerable<string> GetEmailsToSend(int idDiscipline, int sendTo)
        {
            var con = OpenConnection();
            var listEmails = new List<string>();

            try
            {
                string query;
                switch (sendTo)
                {
                    case 0:
                        query = "SELECT s.emailS FROM student s INNER JOIN disciplinestudente ds ON s.idS=ds.idS INNER JOIN discipline d ON ds.idDis = d.idDis WHERE d.idDis = @idDiscipline;";
                        break;

                    case 1:
                        query = "SELECT s.emailS FROM student s INNER JOIN disciplinestudente ds ON s.idS=ds.idS INNER JOIN discipline d ON ds.idDis = d.idDis WHERE d.idDis = @idDiscipline AND ds.delegateDS = 1;";
                        break;
                    default:
                        query = "SELECT s.emailS FROM student s INNER JOIN disciplinestudente ds ON s.idS=ds.idS INNER JOIN discipline d ON ds.idDis = d.idDis WHERE d.idDis = 1;";
                        break;
                }

                MySqlCommand cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);
                MySqlDataReader dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var auxEmail = dataReader.GetString("emailS");
                        listEmails.Add(auxEmail);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return listEmails;
        }

        /// <summary>
        /// Método que devolve o email e a password de um determinado professor;
        /// </summary>
        private static string[] GetEmailAndPasswordTeacher(int idTeacher)
        {
            var dataTeacher = new string[2];

            var con = OpenConnection();

            try
            {
                const string query = "SELECT t.emailT, t.passwordT FROM teacher t WHERE t.idT = @idTeacher;";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idTeacher", idTeacher);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();

                    dataTeacher[0] = dataReader.GetString("emailT");
                    dataTeacher[1] = dataReader.GetString("passwordT");

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return dataTeacher;
        }

        #endregion
    }
}
