﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class SchoolYear
    {
        #region CONST
        
        public SchoolYear()
        {
            this.Id = -1;
            this.Description = "";
            this.Current = -1;
        }

        public SchoolYear(int id, string description, int current)
        {
            this.Id = id;
            this.Description = description;
            this.Current = current;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Description { get; set; }

        public int Current { get; set; }

        #endregion

        #region OTHERS

        
        /// <summary>
        /// Método que devolve todos os anos letivo
        /// </summary>
        public static List<SchoolYear> GetAllSchoolYear()
        {
            List< SchoolYear> schoolYears = new List<SchoolYear>();
            var con = OpenConnection();

            const string query = "SELECT sy.idSY, sy.descriptionSY, sy.currentSY FROM schoolyear sy ORDER BY sy.descriptionSY ASC;";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        SchoolYear schoolYear = new SchoolYear();

                        schoolYear.Id = dataReader.GetInt32("idSY");
                        schoolYear.Description = dataReader.GetString("descriptionSY");
                        schoolYear.Current = dataReader.GetInt32("currentSY");

                        schoolYears.Add(schoolYear);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return schoolYears;
        }

        #endregion
    }
}
