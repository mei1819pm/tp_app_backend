﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Database = TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{

    public class Teacher
    {

        #region CONST

        public Teacher()
        {
            this.Id = 0;
            this.Name = "";
            this.Email = "";
            this.Password = "";
            this.Login = false;
            this.Notification = 0;
            this.Number = "";
            this.University = "";
            this.Graduated = "";
            this.DescriptionDegree = "";
        }

        public Teacher(int id, string name, string email, string password, bool login, int notification, string number, string university, string graduated, string descGraduated)
        {
            this.Id = id;
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Login = login;
            this.Notification = notification;
            this.Number = number;
            this.University = university;
            this.Graduated = graduated;
            this.DescriptionDegree = descGraduated;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public bool Login { get; set; }

        public int Notification { get; set; }

        public string Number { get; set; }

        public string University { get; set; }

        public string Graduated { get; set; }

        public string DescriptionDegree { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que permite verificar Login
        /// </summary>
        public Teacher CheckUser()
        {
            var teacherResponse = new Teacher();
            var con = Database.OpenConnection();

            const string query = "SELECT t.nameT, t.idT, t.emailT, t.notificationsT FROM teacher t  WHERE t.emailT = @email AND t.passwordT = @password AND t.activeT = 1;";

            try
            {
                var cmds = new MySqlCommand(query, con);
                cmds.Parameters.AddWithValue("@email", this.Email);
                cmds.Parameters.AddWithValue("@password", this.Password);
                MySqlDataReader dataReader = cmds.ExecuteReader();

                if (dataReader.HasRows == false)
                {
                    teacherResponse.Login = false;
                }
                else
                {
                    dataReader.Read();                    
                        teacherResponse.Login = true;
                        teacherResponse.Name = dataReader.GetString("nameT");
                        teacherResponse.Id = dataReader.GetInt32("idT");
                        teacherResponse.Email = dataReader.GetString("emailT");
                        teacherResponse.Notification = dataReader.GetInt32("notificationsT");
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }
            return teacherResponse;
        }

        /// <summary>
        /// Método que permite adicionar uma nova conta
        /// </summary>
        /// <returns> 0 não Inseriu, 1 inseriu, 2 existe</returns>
        public int CreateAccount()
        {
            var response = -1;
            var con = Database.OpenConnection();

            try
            {
                if (isExistentUser(this.Email))
                {
                    response = 2;
                  
                }
                else
                {
                    const string query = "INSERT INTO teacher (nameT, phoneNumberT, emailT, passwordT, universityT, graduatedT, descriptionDegreeT, notificationsT, activeT) VALUES (@name, @number, @email, @password, @university, @graduated, @descriptiondegree, @notification, @active);";

                    var cmdInsert = new MySqlCommand(query, con);
                    cmdInsert.Parameters.AddWithValue("@name", this.Name);
                    cmdInsert.Parameters.AddWithValue("@number", this.Number);
                    cmdInsert.Parameters.AddWithValue("@email", this.Email);
                    cmdInsert.Parameters.AddWithValue("@password", this.Password);
                    cmdInsert.Parameters.AddWithValue("@university", this.University);
                    cmdInsert.Parameters.AddWithValue("@graduated", this.Graduated);
                    cmdInsert.Parameters.AddWithValue("@descriptiondegree", this.DescriptionDegree);
                    cmdInsert.Parameters.AddWithValue("@notification", 1);
                    cmdInsert.Parameters.AddWithValue("@active", 1);

                    response = cmdInsert.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }

            return response;
        }

        /// <summary>
        /// Método que devolve um professor
        /// </summary>
        public Teacher GetUserById(int idUser)
        {
            var teacherResponse = new Teacher();
            var con = Database.OpenConnection();

            try
            {
                string query = "SELECT t.idT, t.nameT, t.phoneNumberT, t.emailT, t.passwordT, t.universityT, t.graduatedT, t.descriptionDegreeT, t.notificationsT FROM teacher t WHERE t.idT = @idUser;";
                MySqlCommand cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                MySqlDataReader dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    teacherResponse.Id = dataReader.GetInt32("idT");
                    teacherResponse.Name = dataReader.GetString("nameT");
                    teacherResponse.Number = dataReader.GetString("phoneNumberT");
                    teacherResponse.Email = dataReader.GetString("emailT");
                    teacherResponse.Password = dataReader.GetString("passwordT");
                    teacherResponse.University = dataReader.GetString("universityT");
                    teacherResponse.Graduated = dataReader.GetString("graduatedT");
                    teacherResponse.DescriptionDegree = dataReader.GetString("descriptionDegreeT");
                    teacherResponse.Notification = dataReader.GetInt32("notificationsT");

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }
            return teacherResponse;
        }

        /// <summary>
        /// Método que permite atualizar dados pessoais do professor
        /// </summary>
        public static int UpdatePersonalData(int idUser, Teacher teacherRequest)
        {
            var result = -1;

            var con = Database.OpenConnection();
            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE teacher t SET t.nameT=@name, t.phoneNumberT=@number, t.emailT=@email, t.passwordT=@password  WHERE t.idT = @idUser;"
                };
                cmdUpdate.Parameters.AddWithValue("@idUser", idUser);
                cmdUpdate.Parameters.AddWithValue("@name", teacherRequest.Name);
                cmdUpdate.Parameters.AddWithValue("@email", teacherRequest.Email);
                cmdUpdate.Parameters.AddWithValue("@password", teacherRequest.Password);
                cmdUpdate.Parameters.AddWithValue("@number", teacherRequest.Number);
                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }

            return result;

        }

        /// <summary>
        /// Método que permite atualizar o estado das notificações do professor
        /// </summary>
        public static int UpdateNotification(int idUser, Teacher teacherRequest)
        {
            int result = -1;

            MySqlConnection con = Database.OpenConnection();

            try
            {
                MySqlCommand cmdUpdate = new MySqlCommand();
                cmdUpdate.Connection = con;
                cmdUpdate.CommandText = "UPDATE teacher t SET t.notificationsT=@notification  WHERE t.idT = @idUser;";
                cmdUpdate.Parameters.AddWithValue("@idUser", idUser);
                cmdUpdate.Parameters.AddWithValue("@notification", teacherRequest.Notification);

            result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }

            return result;

        }

        /// <summary>
        /// Método que permite atualizar as informações sobre formação academica do professor
        /// </summary>
        public static int UpdateAcademicFormation(int idUser, Teacher teacherRequest)
        {
            var result = -1;

            var con = Database.OpenConnection();

            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE teacher t SET t.graduatedT=@graduated, t.descriptionDegreeT=@descriptionDegree  WHERE t.idT = @idUser;"
                };
                cmdUpdate.Parameters.AddWithValue("@idUser", idUser);
                cmdUpdate.Parameters.AddWithValue("@graduated", teacherRequest.Graduated);
                cmdUpdate.Parameters.AddWithValue("@descriptionDegree", teacherRequest.DescriptionDegree);

                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                DataBase.DataBase.CloseConnection(con);
            }

            return result;

        }

        /// <summary>
        /// Método que permite atualizar as informações do local de trabalho do professor
        /// </summary>
        public static int UpdateWorkPlace(int idUser, Teacher teacherRequest)
        {
            int result = -1;

            MySqlConnection con = DataBase.DataBase.OpenConnection();

            try
            {
                MySqlCommand cmdUpdate = new MySqlCommand();
                cmdUpdate.Connection = con;
                cmdUpdate.CommandText = "UPDATE teacher t SET t.universityT=@university WHERE t.idT = @idUser;";
                cmdUpdate.Parameters.AddWithValue("@idUser", idUser);
                cmdUpdate.Parameters.AddWithValue("@university", teacherRequest.University);

                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                DataBase.DataBase.CloseConnection(con);
            }

            return result;

        }

        /// <summary>
        /// Método que permite "Elimiar" a conta de um professor
        /// </summary>
        public static int UpdateUserStatus(int idUser)
        {
            int result = -1;

            MySqlConnection con = DataBase.DataBase.OpenConnection();

            try
            {
                MySqlCommand cmdUpdate = new MySqlCommand
                {
                    Connection = con, CommandText = "UPDATE teacher t SET t.activeT=@active WHERE t.idT = @idUser;"
                };
                cmdUpdate.Parameters.AddWithValue("@idUser", idUser);
                cmdUpdate.Parameters.AddWithValue("@active", 0);

                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                DataBase.DataBase.CloseConnection(con);
            }

            return result;

        }

        /// <summary>
        /// Método que permite verificar se já existe email
        /// </summary>
        private static bool isExistentUser(string emailRequest)
        {
            var response = false;
            MySqlConnection con = DataBase.DataBase.OpenConnection();
            string query = "SELECT true FROM teacher t WHERE t.emailT = @email;";

            try
            {

                MySqlCommand cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@email", emailRequest);
                MySqlDataReader dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    var auxReader = dataReader.GetInt32("true");
                    dataReader.Close();

                    if (auxReader == 1)
                    {
                        response = true;
                    }
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                Database.CloseConnection(con);
            }


            return response;
        }

        /// <summary>
        /// Método que devolve o estado de notificações de um Utilizador
        /// </summary>
        public static int GetNotificationStatusById(int idUser)
        {
            int response = 0;
            MySqlConnection con = DataBase.DataBase.OpenConnection();

            try
            {
                string query = "SELECT t.notificationsT FROM teacher t WHERE t.idT = @idUser;";
                MySqlCommand cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                MySqlDataReader dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    response = dataReader.GetInt32("notificationsT");

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                DataBase.DataBase.CloseConnection(con);
            }
            return response;
        }

        /// <summary>
        /// Método que devolve notificações (Aulas, Apoios e Entregas) atraves do id do professot
        /// </summary>
        public static List<Notification> GetNotificationsById(int idUser)
        {
            var notifications = new List<Notification>();

            var lessons = new Lesson();
            var jobDelivery = new JobDelivery();
            var contactHour = new ContactHour();

            notifications.AddRange(Lesson.GetClassForNotification(idUser));
            notifications.AddRange(JobDelivery.GetJobDeliveryForNotification(idUser));
            notifications.AddRange(ContactHour.GetContactHoursForNotification(idUser));

            return notifications;
        }

        #endregion

    }
}
