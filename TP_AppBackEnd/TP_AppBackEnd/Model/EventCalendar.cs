﻿using System;
using System.Collections.Generic;

namespace TP_AppBackEnd.Model
{
    public class EventCalendar
    {
        #region CONST

        public EventCalendar()
        {
            this.Date = "";
            TypeEvent = -1;
        }

        public EventCalendar(string date, int typeEvent)
        {
            this.Date = date;
            this.TypeEvent = typeEvent;
        }

        #endregion

        #region PROPS

        public string Date { get; set; }

        public int TypeEvent { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que devolve eventos (Aulas, Entregas e Apoios) para colocar no calendario do professor
        /// </summary>
        public static List<EventCalendar> GetAllEventsTwoYears(int idUser)
        {
            var events = new List<EventCalendar>();

            var classes = new List<DateTime>();
            var contactHours = new List<DateTime>();
            var jobs = new List<DateTime>();

            var aulas = new Lesson();
            var jobDelivery = new JobDelivery();

            classes.AddRange(Lesson.GetClassTwoYear(idUser));
            contactHours.AddRange(ContactHour.GetContactHoursTwoYear(idUser));
            jobs.AddRange(JobDelivery.GetJobDeliveryTwoYear(idUser));

            //Veficar Aulas
            foreach (var aux in classes)
            {
                var typeB = 0;
                var typeC = 0;

                var auxDate = aux;
                var typeA = 1;

                //Verificar Entregas com aulas
                foreach (var auxJob in jobs)
                {
                    if (DateTime.Compare(auxDate, auxJob) == 0)
                    {
                        typeB = 1;
                        break;
                    }
                }

                //Verificar Apoios com aulas
                foreach (var auxContactHours in contactHours)
                {
                    if (DateTime.Compare(auxDate, auxContactHours) == 0)
                    {
                        typeC = 1;
                        break;
                    }
                }

                switch (typeA)
                {
                    case 1 when typeB == 0 && typeC == 0:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 1));
                        break;
                    case 1 when typeB == 1 && typeC == 1:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 4));
                        break;
                    case 1 when typeB == 1 && typeC == 0:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 5));
                        break;
                    case 1 when typeB == 0 && typeC == 1:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 6));
                        break;
                }

            }

            //Veficar Entregas
            foreach (var aux in jobs)
            {
                var typeA = 0;
                var typeC = 0;

                var auxDate = aux;
                var typeB = 1;

                //Verificar Aulas com enregas
                foreach (var auxJob in classes)
                {
                    if (DateTime.Compare(auxDate, auxJob) == 0)
                    {
                        typeA = 1;
                        break;
                    }
                }

                //Verificar Apoios com entregas
                foreach (var auxContactHours in contactHours)
                {
                    if (DateTime.Compare(auxDate, auxContactHours) == 0)
                    {
                        typeC = 1;
                        break;
                    }
                }

                switch (typeA)
                {
                    case 0 when typeB == 1 && typeC == 0:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 2));
                        break;
                    case 0 when typeB == 1 && typeC == 1:
                        events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 7));
                        break;
                }

            }

            //Veficar Apoios
            foreach (var aux in contactHours)
            {
                var typeA = 0;
                var typeB = 0;

                var auxDate = aux;
                var typeC = 1;

                //Verificar Aulas com Apoios
                foreach (var auxClass in classes)
                {
                    if (DateTime.Compare(auxDate, auxClass) == 0)
                    {
                        typeA = 1;
                        break;
                    }
                }

                //Verificar Entregas com apoios
                foreach (var auxJobs in jobs)
                {
                    if (DateTime.Compare(auxDate, auxJobs) == 0)
                    {
                        typeB = 1;
                        break;
                    }
                }

                if (typeA == 0 && typeB == 0 && typeC == 1)
                {
                    events.Add(new EventCalendar(auxDate.ToString("dd/MM/yyyy"), 3));

                }

            }

            return events;
        }

        #endregion
    }
}
