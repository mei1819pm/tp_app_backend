﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class Discipline
    {
        #region CONST

        public Discipline()
        {
            this.Name = "";
            this.Acronym = "";
            this.IdCourse = -1;
            this.IdSemester = -1;
            this.IdSchoolYear = -1;
            this.IdUser = -1;
            this.Id = -1;
            this.SchoolYear = "";
            this.TimeAttendace = "";
            this.DayAttendace = "";
            this.NameCourse = "";
            this.Semester = "";
            this.Classes = new List<Lesson>();
            this.ContactHours =  new List<ContactHour>();
            this.JobDeliveries = new List<JobDelivery>();
        }

        public Discipline(string name, string acronym, int idCourse, int idSemester, int idSchoolYear, int idUser, int id, string schoolYear, string timeAttendace, string dayAttendace, string nameCourse, string semester, List<Lesson> classes, List<ContactHour> contactHours, List<JobDelivery> jobDeliveries)
        {
            this.Name = name;
            this.Acronym = acronym;
            this.IdCourse = idCourse;
            this.IdSemester = idSemester;
            this.IdSchoolYear = idSchoolYear;
            this.IdUser = idUser;
            this.Id = id;
            this.SchoolYear = schoolYear;
            this.TimeAttendace = timeAttendace;
            this.DayAttendace = dayAttendace;
            this.NameCourse = nameCourse;
            this.Semester = semester;
            this.Classes = classes;
            this.ContactHours = contactHours;
            this.JobDeliveries = jobDeliveries;
        }
        #endregion

        #region PROPS

        public string Name { get; set; }

        public string Acronym { get; set; }

        public int IdCourse { get; set; }

        public int IdSemester { get; set; }

        public int IdSchoolYear { get; set; }

        public int IdUser { get; set; }

        public int Id { get; set; }

        public string SchoolYear { get; set; }

        public string TimeAttendace { get; set; }

        public string DayAttendace { get; set; }

        public string NameCourse { get; set; }

        public string Semester { get; set; }

        public List<Lesson> Classes { get; set; }

        public List<ContactHour> ContactHours { get; set; }

        public List<JobDelivery> JobDeliveries { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que devolve toda planificação de uma deternimada disciplina ou do ano letivo
        /// </summary>
        public static List<Discipline> GetPlanifications (int idUser, int idSchoolYear, int idDiscipline)
        {
            var planifications = new List<Discipline>();     

            var con = OpenConnection();

            try
            {
                Discipline planification;
                Lesson lesson;
                JobDelivery jobDelivery;

                if(idDiscipline == 0)
                {
                    var idDisciplines = new List<int>();
                    idDisciplines.AddRange(GetDisciplineByUserSchoolYear(idUser, idSchoolYear));

                    foreach (var auxIdDiscipline in idDisciplines)
                    {
                        planification = GetInfoDiscipline(auxIdDiscipline);

                        lesson = new Lesson();
                        planification.Classes.AddRange(Lesson.GetClassByDiscipline(auxIdDiscipline));

                        new ContactHour();
                        planification.ContactHours.AddRange(ContactHour.GetContactHourByDiscipline(auxIdDiscipline));

                        jobDelivery = new JobDelivery();
                        planification.JobDeliveries.AddRange(JobDelivery.GetJobDeliveryByDiscipline(auxIdDiscipline));

                        planifications.Add(planification);
                    }

                }
                else
                {
                    planification = GetInfoDiscipline(idDiscipline);
                    
                    lesson = new Lesson();
                    planification.Classes.AddRange(Lesson.GetClassByDiscipline(idDiscipline));

                    new ContactHour();
                    planification.ContactHours.AddRange(ContactHour.GetContactHourByDiscipline(idDiscipline));

                    jobDelivery = new JobDelivery();
                    planification.JobDeliveries.AddRange(JobDelivery.GetJobDeliveryByDiscipline(idDiscipline));

                    planifications.Add(planification);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return planifications;
        }

        /// <summary>
        /// Método que devolve o acronimo de uma disciplina e o ano letivo da mesma
        /// </summary>
        private static Discipline GetInfoDiscipline(int idDiscipline)
        {
            var con = OpenConnection();
            Discipline discipline = null;

            try
            {

                const string query = @"SELECT d.idDis, d.nameDis, d.acronymDis, sy.descriptionSY, d.timeAttendaceDis, d.dayAttendaceDis, se.descriptionS 
	                                    FROM discipline d 
                                        INNER join schoolyear sy
                                        ON d.idSY = sy.idSY 
                                        INNER JOIN semester se
                                        on d.IdS = se.IdS
                                        WHERE d.idDis = @idDiscipline;";

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();

                    discipline = new Discipline()
                    {
                        Id = dataReader.GetInt32("idDis"),
                        Name = dataReader.GetString("nameDis"),
                        Acronym = dataReader.GetString("acronymDis"),
                        SchoolYear = dataReader.GetString("descriptionSY"),
                        TimeAttendace = dataReader.GetDateTime("timeAttendaceDis").ToString("HH:mm"),
                        DayAttendace = dataReader.GetString("dayAttendaceDis"),
                        Semester = dataReader.GetString("descriptionS")
                    };
                    
                    dataReader.Close();
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return discipline;
        }

        /// <summary>
        /// Método que devolve os id das disciplinas que o professor leciona num determinado ano letivo
        /// </summary>
        private static IEnumerable<int> GetDisciplineByUserSchoolYear(int idUser, int idSchoolYear)
        {
            var idDisciplines = new List<int>();
            var con = OpenConnection();

            try
            {

                const string query = "SELECT d.idDis FROM discipline d INNER JOIN schoolyear sy ON d.idSY = sy.idSY INNER JOIN teacher t ON d.idT = t.idT WHERE t.idT = @idUser AND sy.idSY = @idSchoolYear;";

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@idSchoolYear", idSchoolYear);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while(dataReader.Read())
                    {
                        var auxIdDiscipline = dataReader.GetInt32("idDis");
                        idDisciplines.Add(auxIdDiscipline);
                    }       
                    dataReader.Close();
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return idDisciplines;
        }

        /// <summary>
        /// Método que permite adicionar uma nova disciplina
        /// </summary>
        public static int AddDiscipline(Discipline discipline)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "INSERT INTO discipline (nameDis, acronymDis, idC, idSY, idS, idT, timeAttendaceDis, dayAttendaceDis) VALUES (@name, @acronym, @idCourse, @idSchoolYear, @idSemester, @idUser, @timeAttendace, @dayAttendace);";

           try
            {

                var cmdInsert = new MySqlCommand(query, con);
                cmdInsert.Parameters.AddWithValue("@name", discipline.Name);
                cmdInsert.Parameters.AddWithValue("@acronym", discipline.Acronym);
                cmdInsert.Parameters.AddWithValue("@idCourse", discipline.IdCourse);
                cmdInsert.Parameters.AddWithValue("@idSchoolYear", discipline.IdSchoolYear);
                cmdInsert.Parameters.AddWithValue("@idSemester", discipline.IdSemester);
                cmdInsert.Parameters.AddWithValue("@idUser", discipline.IdUser);


                string[] splitHour = discipline.TimeAttendace.Split(":");
                cmdInsert.Parameters.AddWithValue("@timeAttendace", new DateTime(2000,1,1, int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));

                cmdInsert.Parameters.AddWithValue("@dayAttendace", discipline.DayAttendace);

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static IEnumerable<Discipline> GetDisciplineByCourse(int idUser, int idCourse)
        {
            var connection = OpenConnection();
            List<Discipline> disciplines = new List<Discipline>();

            try
            {
                const string query = @"SELECT d.idDis FROM discipline d 
                                        INNER JOIN course c ON d.idC = c.idC 
                                        INNER JOIN teacher t ON d.idT = t.idT 
                                        WHERE t.idT = @idUser AND c.idC = @idCourse;";

                var cmdSelect = new MySqlCommand(query, connection);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@idCourse", idCourse);

                var dataReader = cmdSelect.ExecuteReader();
                var disciplinesIds = new List<int>();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        disciplinesIds.Add(dataReader.GetInt32("idDis"));
                    }
                    dataReader.Close();
                }

                foreach(var id in disciplinesIds)
                {
                    disciplines.Add(GetInfoDiscipline(id));
                }
            }
            catch ( MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(connection);
            }

            return disciplines;
        }

        public static int UpdateDiscipline(int id, Discipline discipline)
        {
            var result = -1;

            var con = OpenConnection();
            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText = @"UPDATE discipline d 
                                    SET d.nameDis=@name, d.acronymDis = @acronim, d.dayAttendaceDis = @day, d.timeAttendaceDis = @hour
                                    WHERE d.idDis = @id;"
                };

                cmdUpdate.Parameters.AddWithValue("@id", id);
                cmdUpdate.Parameters.AddWithValue("@name", discipline.Name);
                cmdUpdate.Parameters.AddWithValue("@acronim", discipline.Acronym);
                cmdUpdate.Parameters.AddWithValue("@day", discipline.DayAttendace);

                string[] splitHour = discipline.TimeAttendace.Split(":");
                cmdUpdate.Parameters.AddWithValue("@hour", new DateTime(2000, 1, 1, int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));

                result = cmdUpdate.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;

        }

        public static int DeleteDiscipline(int id)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM discipline WHERE idDis= @id; ";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@id", id);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que devolve o horario de atendimento de uma determinada disciplina
        /// </summary>
        public static string[] GetTimeAttandace(int idDiscipline)
        {
            var timeAttandace = new string[2];
            var con = OpenConnection();

            const string query = "SELECT d.dayAttendaceDis, d.timeAttendaceDis FROM discipline d WHERE d.idDis = @idDiscipline; ";

            try
            {
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    timeAttandace[0] = dataReader.GetString("dayAttendaceDis");

                    var auxTime = dataReader.GetDateTime("timeAttendaceDis");
                    timeAttandace[1] = auxTime.ToString("HH:mm");

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return timeAttandace;
        }

        /// <summary>
        /// Método que permite copiar o conteudo de uma disciplina para outra (Aulas e Entregas)
        /// </summary>
        public static int CopyDiscipline(int idDisciplineFrom, int idDisciplineTo)
        {
            int result = 1;

            var con = OpenConnection();

            var lessons = new List<Lesson>();
            var jobs = new List<JobDelivery>();

            try
            {

                lessons.AddRange(Lesson.GetClassByDiscipline(idDisciplineFrom));
                foreach (Lesson auxLesson in lessons)
                {
                    auxLesson.IdDiscipline = idDisciplineTo;
                    result = Lesson.AddLesson(auxLesson);
                    if (result != 1)
                    {
                        return result;
                    }

                    int idClass = Lesson.LastId();

                    foreach(LessonContent auLessonContent in auxLesson.ClassContent)
                    {
                        auLessonContent.IdClass = idClass;
                        result = LessonContent.AddContent(auLessonContent);
                        if (result != 1)
                        {
                            return result;
                        }
                    }

                    foreach (LessonMaterial auLessonMaterial in auxLesson.ClassMaterial)
                    {
                        auLessonMaterial.IdClass = idClass;
                        result = LessonMaterial.AddMaterial(auLessonMaterial);
                        if (result != 1)
                        {
                            return result;
                        }
                    }

                }

                jobs.AddRange(JobDelivery.GetJobDeliveryByDiscipline(idDisciplineFrom));
                foreach(JobDelivery auxJob in jobs)
                {
                    auxJob.IdDiscipline = idDisciplineTo;
                    result = JobDelivery.AddJobDelivery(auxJob);
                    if(result !=1)
                    {
                        return result;
                    }
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        #endregion
    }
}
