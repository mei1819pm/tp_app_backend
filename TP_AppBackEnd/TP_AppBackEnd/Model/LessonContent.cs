﻿using MySql.Data.MySqlClient;
using System;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class LessonContent
    {
        #region CONST

        public LessonContent()
        {
            this.Id = 0;
            this.Description = "";


        }

        public LessonContent(int id, string description)
        {
            this.Id = id;
            this.Description = description;

        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Description { get; set; }

        public int IdClass { get; set; }
        #endregion

        #region OTHER

        public static int AddContent(LessonContent content)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "INSERT INTO classcontent (descriptionCC, idCl) " +
                "VALUES (@description, @idClass);";

            try
            {

                var cmdInsert = new MySqlCommand(query, con);

                cmdInsert.Parameters.AddWithValue("@idClass", content.IdClass);
                cmdInsert.Parameters.AddWithValue("@description", content.Description);

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        public static int DeleteContent(int id)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM classcontent WHERE idCC = @id;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@id", id);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }
        #endregion
    }
}
