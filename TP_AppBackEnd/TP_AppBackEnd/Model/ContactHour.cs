﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using static TP_AppBackEnd.DataBase.DataBase;

namespace TP_AppBackEnd.Model
{
    public class ContactHour
    {
        #region CONST

        public ContactHour()
        {
            this.Id = 0;
            this.Day = "";
            this.Hour = "00:00";
            this.Type = "";
            this.AppOrRoom = "";
            this.Description = "";
            this.Discipline = "";
            this.Course = "";
            this.NameStudent = "";
            this.IdDiscipline = -1;
            this.IdStudent = -1;
            this.IdTeacher = -1;
        }

        public ContactHour(int id, string day, string hour, string type, string appOrRoom, string description, string discipline, string course, string student, int idDiscipline, int idStudent, int idTeacher)
        {
            this.Id = id;
            this.Day = day;
            this.Hour = hour;
            this.Type = type;
            this.AppOrRoom = appOrRoom;
            this.Description = description;
            this.Discipline = discipline;
            this.Course = course;
            this.NameStudent = student;
            this.IdDiscipline = idDiscipline;
            this.IdStudent = idStudent;
            this.IdTeacher = idTeacher;
        }

        #endregion

        #region PROPS

        public int Id { get; set; }

        public string Day { get; set; }

        public string Hour { get; set; }

        public string Type { get; set; }

        public string AppOrRoom { get; set; }

        public string Description { get; set; }

        public string Discipline { get; set; }

        public string Course { get; set; }

        public string NameStudent { get; set; }

        public int IdDiscipline { get; set; }

        public int IdStudent { get; set; }

        public int IdTeacher { get; set; }

        #endregion

        #region OTHERS

        /// <summary>
        /// Método que devolve os apoios do dia
        /// </summary>
        public static List<ContactHour> GetContactHoursToday(int idUser)
        {
            var contactHours = new List<ContactHour>();
            var con = OpenConnection();

            try
            {
                const string query = "SELECT c.idCH, c.dateCH, c.hourCH, c.typeCH, c.appOrRoomCH, c.descriptionCH, d.nameDis, s.nameS, cou.nameC FROM contacthour c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT INNER JOIN student s ON c.idS = s.idS WHERE t.idT = @idUser AND c.dateCH = date(now()) AND c.hourCH > now() ORDER BY c.hourCH ASC;";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {   
                    while (dataReader.Read())
                    {
                        var contact = new ContactHour {Id = dataReader.GetInt32("idCH")};

                        var auxDate = dataReader.GetDateTime("dateCH");
                        contact.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("hourCH");
                        contact.Hour = auxTime.ToString("HH:mm");

                        contact.Type = dataReader.GetString("typeCH");
                        contact.AppOrRoom = dataReader.GetString("appOrRoomCH");
                        contact.Description = dataReader.GetString("descriptionCH");
                        contact.Discipline = dataReader.GetString("nameDis");
                        contact.Course = dataReader.GetString("nameC");
                        contact.NameStudent = dataReader.GetString("nameS");

                        contactHours.Add(contact);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }
            return contactHours;
        }

        /// <summary>
        /// Método que devolve Todos os apoios de uma disciplina
        /// </summary>
        public static List<ContactHour> GetContactHourByDiscipline(int idDiscipline)
        {
            var contactHours = new List<ContactHour>();
            var con = OpenConnection();

            try
            {
                const string query = "SELECT c.idCH, c.dateCH, c.hourCH, c.typeCH, c.appOrRoomCH, c.descriptionCH, d.nameDis, s.nameS, cou.nameC FROM contacthour c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT INNER JOIN student s ON c.idS = s.idS WHERE d.idDis = @idDiscipline ORDER BY c.hourCH ASC;";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idDiscipline", idDiscipline);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var contact = new ContactHour {Id = dataReader.GetInt32("idCH")};

                        var auxDate = dataReader.GetDateTime("dateCH");
                        contact.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("hourCH");
                        contact.Hour = auxTime.ToString("HH:mm");

                        contact.Type = dataReader.GetString("typeCH");
                        contact.AppOrRoom = dataReader.GetString("appOrRoomCH");
                        contact.Description = dataReader.GetString("descriptionCH");
                        contact.Discipline = dataReader.GetString("nameDis");
                        contact.Course = dataReader.GetString("nameC");
                        contact.NameStudent = dataReader.GetString("nameS");

                        contactHours.Add(contact);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return contactHours;
        }

        /// <summary>
        /// Método que devolve as notificaçoes das entregas que terão inicio brevemente
        /// </summary>
        public static IEnumerable<Notification> GetContactHoursForNotification(int idUser)
        {
            var notifications = new List<Notification>();
            var con = OpenConnection();
            const int pre = 3;

            try
            {
                const string query = "SELECT c.idCH, c.hourCH, c.appOrRoomCH, c.dateCH, d.nameDis, s.nameS FROM contacthour c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT INNER JOIN student s ON c.idS = s.idS WHERE t.idT = @idUser AND c.hourCH BETWEEN now() AND DATE_ADD(now(), INTERVAL 1 HOUR);";
                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var notification = new Notification {Id = dataReader.GetInt32("idCH")};

                        notification.Id = int.Parse(pre.ToString() + notification.Id);

                        var auxDate = dataReader.GetDateTime("dateCH");
                        notification.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("hourCH");
                        notification.Hour = auxTime.ToString("HH:mm");

                        notification.Discipline = dataReader.GetString("nameDis");
                        notification.RoomOrMethod = dataReader.GetString("appOrRoomCH");

                        notification.Student = dataReader.GetString("nameS");

                        notification.Title = "Apoio";

                        notifications.Add(notification);
                    }
                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return notifications;
        }

        /// <summary>
        /// Método que devolde todos os dias em que tem Apoios marcados no ultimo e proximo ano
        /// </summary>
        public static IEnumerable<DateTime> GetContactHoursTwoYear(int idUser)
        {

            var contactHours = new List<DateTime>();
            var con = OpenConnection();

            try
            {
                const string query = "SELECT DISTINCT(c.dateCH) FROM contacthour c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN teacher t ON d.idT = t.idT INNER JOIN student s ON c.idS = s.idS WHERE t.idT = @idUser AND c.hourCH BETWEEN DATE_ADD(DATE(now()), INTERVAL -1 Year) AND DATE_ADD(DATE(now()), INTERVAL 1 Year);";

                var cmdSelect = new MySqlCommand(query, con);
                cmdSelect.Parameters.AddWithValue("@idUser", idUser);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var auxTime = dataReader.GetDateTime("dateCH");

                        contactHours.Add(auxTime);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return contactHours;
        }

        /// <summary>
        /// Método que devolve as Entregas de um determinado dia
        /// </summary>
        public static List<ContactHour> GetContactHourByDay(int idUser, string date)
        {
            var contactHours = new List<ContactHour>();
            var con = OpenConnection();

            try
            {
                var day = DateTime.Parse(date);

                const string query = "SELECT c.idCH, c.dateCH, c.hourCH, c.typeCH, c.appOrRoomCH, c.descriptionCH, d.nameDis, s.nameS, cou.nameC FROM contacthour c INNER JOIN discipline d ON c.idDis = d.idDis INNER JOIN course cou ON d.idC = cou.idC INNER JOIN teacher t ON d.idT = t.idT INNER JOIN student s ON c.idS = s.idS WHERE t.idT = @idUser AND c.dateCH = @date ORDER BY c.hourCH ASC;";

                var cmdSelect = new MySqlCommand(query, con);

                cmdSelect.Parameters.AddWithValue("@idUser", idUser);
                cmdSelect.Parameters.AddWithValue("@date", day);

                var dataReader = cmdSelect.ExecuteReader();

                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var contact = new ContactHour {Id = dataReader.GetInt32("idCH")};

                        var auxDate = dataReader.GetDateTime("dateCH");
                        contact.Day = auxDate.ToString("dd/MM/yyyy");

                        var auxTime = dataReader.GetDateTime("hourCH");
                        contact.Hour = auxTime.ToString("HH:mm");

                        contact.Type = dataReader.GetString("typeCH");
                        contact.AppOrRoom = dataReader.GetString("appOrRoomCH");
                        contact.Description = dataReader.GetString("descriptionCH");
                        contact.Discipline = dataReader.GetString("nameDis");
                        contact.Course = dataReader.GetString("nameC");
                        contact.NameStudent = dataReader.GetString("nameS");

                        contactHours.Add(contact);
                    }

                    dataReader.Close();
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return contactHours;
        }

        /// <summary>
        /// Método que premite eliminar uma hora de contacto
        /// </summary>
        public static int DeleteContactHour(int idContactHour)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "DELETE FROM contacthour WHERE idCH = @contact;";

            try
            {
                var cmdDelete = new MySqlCommand(query, con);
                cmdDelete.Parameters.AddWithValue("@contact", idContactHour);

                result = cmdDelete.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite adicionar uma nova entrega
        /// </summary>
        public static int AddContactHour(ContactHour contactHour)
        {
            int result = -1;

            var con = OpenConnection();

            const string query = "INSERT INTO contactHour (dateCH, hourCH, typeCH, appOrRoomCH, descriptionCH, idDis, idS, idT, notificationIssueCH) " +
                "VALUES (@date, @hour, @type, @appRoom, @description, @idDiscipline, @idStudent, @idTeacher, 0);";

            try
            {

                var cmdInsert = new MySqlCommand(query, con);

                string[] splitDate = contactHour.Day.Split("/");
                string[] splitHour = contactHour.Hour.Split(":");

                cmdInsert.Parameters.AddWithValue("@date", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                cmdInsert.Parameters.AddWithValue("@hour", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));

                cmdInsert.Parameters.AddWithValue("@type", contactHour.Type);
                 cmdInsert.Parameters.AddWithValue("@appRoom", contactHour.AppOrRoom);
                 cmdInsert.Parameters.AddWithValue("@description", contactHour.Description);
                 cmdInsert.Parameters.AddWithValue("@idDiscipline", contactHour.IdDiscipline);
                 cmdInsert.Parameters.AddWithValue("@idStudent", contactHour.IdStudent);
                 cmdInsert.Parameters.AddWithValue("@idTeacher", contactHour.IdTeacher); 

                result = cmdInsert.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;
        }

        /// <summary>
        /// Método que permite atualizar os dados de um um determinado apoio
        /// /// </summary>
        public static int UpdateContactHour(int idContactHour, ContactHour contactHour)
        {
            var result = -1;

            var con = OpenConnection();
            try
            {
                var cmdUpdate = new MySqlCommand
                {
                    Connection = con,
                    CommandText =
                        "UPDATE contactHour SET dateCH = @date, hourCH = @hour, typeCH = @type, appOrRoomCH = @appRoom, descriptionCH = @description WHERE idCH = @idContactHour; "
                };

                string[] splitDate = contactHour.Day.Split("/");
                string[] splitHour = contactHour.Hour.Split(":");

                cmdUpdate.Parameters.AddWithValue("@date", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                cmdUpdate.Parameters.AddWithValue("@hour", new DateTime(int.Parse(splitDate[2]), int.Parse(splitDate[1]), int.Parse(splitDate[0]), int.Parse(splitHour[0]), int.Parse(splitHour[1]), 00));
                 cmdUpdate.Parameters.AddWithValue("@type", contactHour.Type);
                 cmdUpdate.Parameters.AddWithValue("@appRoom", contactHour.AppOrRoom);
                 cmdUpdate.Parameters.AddWithValue("@description", contactHour.Description);
                 cmdUpdate.Parameters.AddWithValue("@idContactHour", idContactHour); 

                result = cmdUpdate.ExecuteNonQuery();


            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection(con);
            }

            return result;

        }
        #endregion
    }
}
