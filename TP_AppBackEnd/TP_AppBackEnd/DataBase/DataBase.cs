﻿using System;
using MySql.Data.MySqlClient;

namespace TP_AppBackEnd.DataBase
{
    public static class DataBase
    {
        private const string Server = "localhost";
        private const string Database = "appandroid";
        private const string Uid = "root";
        private const string Password = "root";
        private const string ConnectionString = "SERVER=" + Server + ";" + "DATABASE=" + Database + ";" + "UID=" + Uid + ";" + "PASSWORD=" + Password + ";SslMode=none";


        /// <summary>
        /// Inicia Ligacao a Base de Dados
        /// </summary>
        /// <returns>1 caso sucesso</returns>
        public static MySqlConnection OpenConnection()
        {
            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(ConnectionString);
                if (connection.State != System.Data.ConnectionState.Open)
                {
                    connection.Open();
                }
                else
                {
                    throw  new Exception("Connection already opened");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return connection;
        }


        public static void CloseConnection(MySqlConnection con)
        {
            try
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                    con.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
